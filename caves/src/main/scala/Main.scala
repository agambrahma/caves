

sealed trait Cell
case object Space extends Cell
case object Wall extends Cell

case class Params(r1_cutoff: Int, r2_cutoff: Option[Int])

object Main extends App {
  val randGen = scala.util.Random

  val numRows = 50
  val numCols = 70
  val wallProbabilityPercent = 40

  var grid = Array.fill[Cell](numRows, numCols)(Space)

  def showGrid = {
    println()
    for (i <- 0 until numRows) {
      for (j <- 0 until numCols) {
        grid(i)(j) match {
          case Space => print(".")
          case Wall => print("#")
        }
      }
      println()
    }
    println()
  }

  def seedCaves() = {
    // Helpers.
    def isEdge(row: Int, col: Int): Boolean = {
      (row == 0) ||
      (col == 0) ||
      (row == numRows - 1) ||
      (col == numCols - 1)
    }
    def isMiddleRow(row: Int): Boolean = {
      row == numRows / 2
    }
    def shouldRandomlyPlaceWall(): Boolean = {
      randGen.nextInt(100) <= wallProbabilityPercent
    }

    for (i <- 0 until numRows) {
      for (j <- 0 until numCols) {
        grid(i)(j) = {
          if (isEdge(i,j)) {
            Wall
          } else if (isMiddleRow(i)) {
            Space
          } else if (shouldRandomlyPlaceWall()) {
            Wall
          } else {
            Space
          }
        }
      }
    }
  }

  // Helper
  def getNeighborCount(r: Int, c: Int, delta: Int): Int = {
    var n = 0

    for (i <- (r - delta) to (r + delta) if (i >= 0 && i < numRows)) {
      for (j <- (c - delta) to (c + delta) if (j >= 0 && j < numCols)) {
        // Skip corners when delta > 1.
        val shouldSkip = (delta > 1 && math.abs(i-r) == delta && math.abs(j-c) == delta)
        if (!shouldSkip) {
          grid(i)(j) match {
            case Wall => n += 1
            case Space =>
          }
        }
      }
    }

    n
  }

  // Helper
  def applyCellRules(r1: Int, r2: Int, params: Params): Cell = {
    if (r1 >= params.r1_cutoff) {
      return Wall
    }
    if (r2 <= params.r2_cutoff.getOrElse(-1)) {
      return Wall
    }
    // Else.
    return Space
  }

  // Apply rules, given each cell and its surroundings.
  def iterateCaves(params: Params) = {
    val newGrid = grid

    // Skip edges during iteration
    for (i <- 1 until (numRows-1)) {
      for (j <- 1 until (numCols-1)) {
        val numNeighbors1 = getNeighborCount(i, j, 1)
        val numNeighbors2 = getNeighborCount(i, j, 2)

        val newCell = applyCellRules(numNeighbors1, numNeighbors2, params)
        // println(s"Debug: computed $numNeighbors1, $numNeighbors2 for ($i, $j)")
        newGrid(i)(j) = newCell
      }
    }

    newGrid
  }

  def runMain = {
    seedCaves
    showGrid

    // Two rounds of "coalescing", larger then smaller islands.
    val round_1 = Params(5, Some(6))
    val round_2 = Params(5, Some(3))

    // One round of pruning out the isolated walls.
    val round_3 = Params(5, None)

    val params = List.fill(4)(round_1) ++ List.fill(3)(round_2) ++ List.fill(1)(round_3)

    // Later: params.foldLeft(grid)( (g: Array[Cell], param: Param) => 
    for (param <- params) {
      grid = iterateCaves(param)
      showGrid
    }
  }

  println("Welcome to my caves")
  runMain
}
